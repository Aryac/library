#ifndef STUDENT_H
#define STUDENT_H
#include <iostream>

using namespace std;

class Student
{
protected:
    string stdname_ ;
    int stdnum_ ;
public:

    Student();
  void enterStudent ();
  void printStudent();
  string getStdName() ;


};


#endif // STUDENT_H
