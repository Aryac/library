#ifndef BOOK_H
#define BOOK_H
#include <string>
using namespace std;
class Book
{
protected:

    int year_ ;
    string bookname_;
    string writer_;

public:

    Book();
    void enterbook ();
    void set_date();
    void printBook() ;
    string getBook () ;

};



#endif // BOOK_H
