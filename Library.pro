TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        main.cpp \
    book.cpp \
    student.cpp \
    mainlibrary.cpp

HEADERS += \
    book.h \
    student.h \
    mainlibrary.h
